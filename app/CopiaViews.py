from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import FormView, ListView
from app.models import FileParser
from app.forms import FileParserExportForm
from django.urls import reverse_lazy
from app.resources import FileParserResource
from datetime import datetime

"""
class ReportView(FormView):
    model = FileParser
    template_name = 'index.html'
    form_class = FileParserExportForm
    success_url = ''

    def form_valid(self, form):
        now = datetime.now()
        fileparse_resource = FileParserResource()
        date_range = (form.cleaned_data['init_date'], form.cleaned_data['finish_date'])
        if form.cleaned_data['filter_by'] == 'creation_date':
            queryset = FileParser.objects.filter(created_at__range=date_range, customer=form.cleaned_data['customer']) \
                .filter(sent=form.cleaned_data['sent']).order_by('created_at')
        else:
            queryset = FileParser.objects.filter(email_date__range=date_range, customer=form.cleaned_data['customer']) \
                .filter(sent=form.cleaned_data['sent']).order_by('email_date')

        dataset = fileparse_resource.export(queryset)
        dt_string = now.strftime("%d-%m-%Y %H:%M:%S")
        response = HttpResponse(dataset.xlsx, content_type='application/vnd.ms-excel')
        # response['target'] = reverse('app:index')
        response['Content-Disposition'] = 'attachment; filename="report_' + dt_string + '.xlsx"'
        return response

    def get_success_url(self):
        return HttpResponseRedirect(reverse_lazy('app:index'))

    def form_invalid(self, form):
        print('SQL injection attempt')
        return super(ReportView, self).form_invalid(form)
"""


class ReportView(FormView):
    model = FileParser
    template_name = 'index.html'
    form_class = FileParserExportForm
    success_url = ''

    def form_valid(self, form):

        date_range = (form.cleaned_data['init_date'], form.cleaned_data['finish_date'])
        if form.cleaned_data['filter_by'] == 'creation_date':
            queryset = FileParser.objects.filter(created_at__range=date_range,
                                                 customer=form.cleaned_data['customer']) \
                .filter(sent=form.cleaned_data['sent']).order_by('created_at')
        else:
            queryset = FileParser.objects.filter(email_date__range=date_range,
                                                 customer=form.cleaned_data['customer']) \
                .filter(sent=form.cleaned_data['sent']).order_by('email_date')

        return ReportList.as_view()(self.request, queryset)


class ReportList(ListView):
    model = FileParser
    template_name = 'report_list.html'
    context_object_name = 'report_list'
    paginate_by = 20
    success_url = 'index.html'

    def post(self, request, *args, **kwargs):
        self.queryset = args[0]
        return self.get(request, *args, **kwargs)


