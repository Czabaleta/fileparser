from import_export import resources
from app.models import FileParser


class FileParserResource(resources.ModelResource):
    class Meta:
        model = FileParser
        fields = ('customer__name', 'email_date', 'customer__parser', 'options', 'upload_file', 'created_at',)
        export_order = ('customer__name', 'email_date', 'customer__parser', 'options', 'upload_file', 'created_at',)
