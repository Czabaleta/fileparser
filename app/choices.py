
TopDownParser = 'Top-Down-Parser'
BottomUpParser = 'Bottom-Up-Parser'
Backtracking = 'Backtracking'

ByEmail = 'By Email'
UploadedByUser = 'Uploaded By User'

select_parser = (
        ('', 'Choose...'),
        (TopDownParser, 'Top-Down-Parser'),
        (BottomUpParser, 'Bottom-Up-Parser'),
        (Backtracking, 'Backtracking')
    )

select_option = (
        ('', 'Choose...'),
        (ByEmail, 'By Email'),
        (UploadedByUser, 'Uploaded By User'),
    )
