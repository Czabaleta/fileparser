from django.db import models

from app.choices import select_parser, TopDownParser, select_option, ByEmail


class Customers(models.Model):
    id_customer = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)  # Default: null=False, blank=False ,o sea, campos requeridos
    parser = models.CharField(max_length=20, choices=select_parser, default=TopDownParser)

    class Meta:
        ordering = ['name', 'parser']

    def __str__(self):
        return f'{self.name}'


class FileParser(models.Model):
    customer = models.ForeignKey('Customers', on_delete=models.CASCADE)
    email_id = models.SmallIntegerField()
    email_date = models.DateTimeField()
    options = models.CharField(max_length=20, choices=select_option, default=ByEmail)
    sent = models.BooleanField(null=False, blank=False, default=False)
    upload_file = models.FileField(upload_to='uploads/')
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['email_id']

    def __str__(self):
        return f'{self.email_id}, {self.email_date}, {self.options}, {self.created_at}, {self.customer.name}, ' \
               f'{self.customer.parser}'
