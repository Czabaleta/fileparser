from django import forms

from app import choices
from app.models import Customers, FileParser


class CustomersForm(forms.ModelForm):
    class Meta:
        model = Customers
        fields = ['name', 'parser']
        labels = {'name': 'Name',
                  'parser': 'Parser Type'}
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}),
                   'parser': forms.Select(choices=choices.select_parser, attrs={'class': 'form-control'})
                   }


class FileParserForm(forms.ModelForm):
    class Meta:
        model = FileParser
        fields = ['customer', 'email_id', 'email_date', 'options', 'sent', 'upload_file']

        labels = {'customer': 'Customer', 'email_id': 'Email ID', 'email_date': 'Email Date', 'options': 'Options',
                  'sent': 'Send Status', 'upload_file': 'Upload File'}

        widgets = {'customer': forms.Select(attrs={'class': 'form-control', 'required': 'required'}),
                   'email_id': forms.TextInput(attrs={'class': 'form-control'}),
                   'email_date': forms.DateTimeField(),
                   'options': forms.Select(choices=choices.select_option, attrs={'class': 'form-control'}),
                   'sent': forms.CheckboxInput(attrs={'class': 'form-control'}),
                   'upload_file': forms.FileField(),
                   }


class FileParserExportForm(forms.ModelForm):
    init_date = forms.DateField()
    finish_date = forms.DateField()
    filter_by = forms.ChoiceField(choices=(('email_date', 'By email date'), ('creation_date', 'By creation date')))

    class Meta:
        model = FileParser
        fields = ['customer', 'sent']

        labels = {'customer': 'Customer', 'sent': 'Send Status'}

        widgets = {'customer': forms.Select(attrs={'class': 'form-control'}),
                   'sent': forms.CheckboxInput(),
                   }

