from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from app.models import Customers, FileParser


# Register your models here.
@admin.register(Customers)
class CustomerAdmin(ImportExportModelAdmin):
    pass


@admin.register(FileParser)
class FileParserAdmin(ImportExportModelAdmin):
    pass
