# Generated by Django 2.2.1 on 2019-06-11 14:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0010_auto_20190611_1110'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fileparser',
            name='sent',
            field=models.BooleanField(default=False),
        ),
    ]
