from django.urls import path
from app.views import ReportView

app_name = 'app'
urlpatterns = [
    path('',  ReportView.as_view(), name='index'),
    # path('report/',  ReportList.as_view(), name='report'),

]
